---
title: "全链路性能测试SuperSheeps用于Web服务器性能测试"
---
# SuperSheeps用于Web服务器性能测试

&emsp;&emsp;Supresheeps支持测试http(s)和websocket服务,可用于网站、H5游戏等服务器性能测试

### 搭建环境：windows11

## 一、工具下载：

获取supersheeps最新版本的zip包  
下载途径1. <http://www.supersheeps.cn/release>  
下载途径2. <https://gitee.com/lutianming/supersheeps_extend/releases>

## 二、安装部署：

1. 解压supersheeps工具zip包，第三方安全软件可能误杀请关闭第三方安全软件，或者添加白名单。
2. 双击supersheeps.exe启动，访问<http://127.0.0.1:1080>

## 三、准备用例：

1. 设置http(s)代理  
&emsp;&emsp;以笔者windows11为例，进入系统设置——网络和Internet——代理——手动设置代理，点击“设置”，打开使用代理服务器开关，代理IP地址设置为127.0.0.1，端口为1080，点击保存  
注：实际情况可能使用不同的方式和工具代理目标程序

2. 开启用例录制  
&emsp;&emsp;浏览器访问 <http://127.0.0.1:1080>,进入用例管理，开启录制功能

3. 开始录制用例  
&emsp;&emsp;浏览器访问需要压测的网站，正常完成页面操作

4. 保存用例  
&emsp;&emsp;操作完成后,浏览器访问 <http://127.0.0.1:1080>,进入用例管理，关闭录制功能，删除用例中不需要的域名和ip。填写备注，然后点击保存。在用例列表中即可看到录制的用例

## 四、脚本开发

1. 配置调试任务  
浏览器访问 <http://127.0.0.1:1080>,进入任务管理，填写好任务配置信息，**总人数设置为1人**，选择准备好的测试用例，勾选需要测试的IP地址或者域名，然后点击添加，在下方面任务列表看到添加的任务。

2. 脚本  
默认脚本目录：project/lua/0/script  
--main.lua   脚本入口  
--router.lua   接口路由，处理接口上下文  

3. 插件使用示例[main.lua]
```lua
local http_controller = require "http_plugin.controller"
local router = require "router"

function EventStart(task)
	Log(LOG_NORMAL,"EventStart", task)
    local paly_fast = false
    local run_count = 1
    local params = task.params
    if params.PlayFast then paly_fast = true end
    if params.RunCount then run_count = tonumber(params.RunCount) end
    http_controller.init(router, paly_fast, run_count)  --初始化，设置路由、播放模式、及执行次数
end

function EventConnectOpen(ip, port, protocol)
    Log(LOG_DEBUG,"EventConnectOpen")
    http_controller.connection_open(ip, port, protocol)
end

function EventConnectClose(ip, port, protocol)
    Log(LOG_DEBUG,"EventConnectClose")
    http_controller.connection_close(ip, port, protocol)
end

function EventConnectSend(ip, port, data, protocol)
    Log(LOG_DEBUG,"EventConnectSend")
	http_controller.connection_send(ip, port, data, protocol)
end

function EventConnectMade(hsock)
    Log(LOG_DEBUG,"EventConnectMade")
    http_controller.connection_made(hsock)
end

function EventConnectFailed(hsock, err)
    Log(LOG_DEBUG,"EventConnectFailed")
    http_controller.connection_failed(hsock)
end

function EventConnectClosed(hsock, err)
    Log(LOG_DEBUG,"EventConnectClosed")
    http_controller.connection_closed(hsock, err)
end

function EventConnectRecved(hsock, data)
    Log(LOG_DEBUG,"EventConnectRecved data, len", #data)
    --Log(3, "Tcp Socket Connect Recved:", #data)
    http_controller.connection_recved(hsock, data)
end

function EventTimeOut()
    --Log(LOG_DEBUG,"EventStart")
    http_controller.timeout()
end

function EventStop(msg)
	Log(LOG_FAULT,"EventStop:",msg)
end
```

4. 插件初始化  
supersheeps默认使用正常回放模式，即两个用例步骤之间的时间间隔与录制时保持一致，在插件初始化时可以选择使用快进模式
，同时用户传递一个router作为接口路由处理上下文逻辑，run_count控制用户循环次数

```lua
function EventStart(task)
	Log(LOG_NORMAL,"EventStart", task)
    local paly_fast = false
    local run_count = 1
    local params = task.params
    if params.PlayFast then paly_fast = true end
    if params.RunCount then run_count = tonumber(params.RunCount) end
    http_controller.init(router, paly_fast, run_count)  --初始化，设置路由、播放模式、及执行次数
end
```

5. 接口路由及上下文处理[router.lua]

注意：仅仅在需要处理上下文逻辑的时候才需要编写路由处理代码，不处理则按照录制的数据直接发送

```lua
local router = {}

router["/"] = function(conn, req)  --接口路由，即使不写该请求也会被默认处理
    Log(LOG_NORMAL, "http请求：", req)
    --PlayPause()   --发送请求之后暂停执行，等待本次返回
    --conn.ssl = true  --启用ssl连接

    conn.on_response = function(conn, res)  --http接口返回回调
        Log(LOG_NORMAL, "http 返回:", res.status_code, res.status)
		--PlayNormal()  --接收返回后继续执行
    end

    conn.on_ws_request = function(conn, message)   --websocket发送回调
        Log(LOG_NORMAL, "websocket 发送:", message)
        return message
    end
    
    conn.on_ws_response = function(conn, message)   --websocket接收回调
        Log(LOG_NORMAL, "websocket 接收:", message)
    end

    conn.on_close = function(conn, err)  --连接关闭回调函数
        Log(LOG_NORMAL, "http关闭:", err)
    end

end

-- router["*"] = function(http, req)   --这个路由用于接收所有未知的接口，用户可以更加自由的组织自己的代码
--     print(req.uri)
-- end

return router
```

6. 调整定时器  
    sheeps.ini文件中可以调整定时器时间周期，根据项目需要调整到合理的数值，可以在快进模式下加快执行效率，正常播放时用于调整负载端尽可能少得出现空转。

## 五、测试执行：

1. 配置任务  
浏览器访问 <http://127.0.0.1:1080>,进入任务管理，填写好任务配置信息，选择准备好的测试用例，勾选需要测试的IP地址或者域名，然后点击添加，在下方面任务列表看到添加的任务。

2. 执行任务  
在上一步添加的任务行点击，点击开始

3. 实时报表  
在任务列表点击查看，进入实时报表页面

## 六、分析报告

1.根据各个维度的信息分析服务器性能，以及排查服务器错误
