/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#include "sheeps.h"
#if !defined(__WINDOWS__) && (defined(WIN32) || defined(WIN64) || defined(_MSC_VER) || defined(_WIN32))
#define __WINDOWS__
#endif

#ifdef __WINDOWS__
#ifdef _DEBUG
#ifndef _WIN64
#pragma comment(lib,"..\\Win32\\sheeps\\Debug\\Sheeps.lib")
#else
#pragma comment(lib, "..\\X64\\sheeps\\Debug\\Sheeps.lib")
#endif // _WIN32
#else
#ifndef _WIN64
#pragma comment(lib, "..\\Win32\\sheeps\\release\\Sheeps.lib")
#else
#pragma comment(lib, "..\\X64\\sheeps\\release\\Sheeps.lib")
#endif // _WIN32
#endif
#endif // __WINDOWS__

int main(int argc, char* argv[])
{
    return run_with_parms(argc, argv);
}