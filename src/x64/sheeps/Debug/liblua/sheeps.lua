--
-- 1.此文件属于supersheeps框架lua模式的一部分，用于lua虚拟机预处理，用于优化运行效率，并定义了两个函数
-- 2.lua虚拟机创建完成后会先执行此文件
-- 3.global_vm_init用于对全局虚拟机的预处理
-- 4.user_vm_init用于对用户虚拟机的预处理
-- 5.一般情况下用户不需要关心这个文件
--

function global_vm_init(projectid)
end

function user_vm_init(projectid)
end