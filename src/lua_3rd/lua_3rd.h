#pragma once
#include "lua.hpp"

#if !defined(__WINDOWS__) && (defined(WIN32) || defined(WIN64) || defined(_MSC_VER) || defined(_WIN32))
#define __WINDOWS__
#endif

#ifdef __cplusplus
extern "C"
{
#endif

	LUAMOD_API int luaopen_sproto_core(lua_State* L);
	LUAMOD_API int luaopen_client_crypt(lua_State* L);

	LUALIB_API int luaopen_pb(lua_State* L);
	LUALIB_API int luaopen_pb_unsafe(lua_State* L);
	LUALIB_API int luaopen_pb_io(lua_State* L);
	LUALIB_API int luaopen_pb_conv(lua_State* L);
	LUALIB_API int luaopen_pb_buffer(lua_State* L);
	LUALIB_API int luaopen_pb_slice(lua_State* L);

	LUALIB_API void luaL_open_3rd_lib(lua_State* L);
	LUALIB_API void luaL_open_3rd_server_lib(lua_State* L);

#ifdef __cplusplus
}
#endif