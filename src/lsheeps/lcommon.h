/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#ifndef __LCOMMON_H__
#define __LCOMMON_H__
#include "stdio.h"
#include "lua.hpp"
#include "AgentManager.h"
#include "mycrypto.h"
#include "Config.h"

int register_common_api(lua_State* L);

#endif // !__LCOMMON_H__