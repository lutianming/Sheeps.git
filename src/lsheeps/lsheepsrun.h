/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#ifndef __LSHEEPS_RUN_H__
#define __LSHEEPS_RUN_H__

#if !defined(__WINDOWS__) && (defined(WIN32) || defined(WIN64) || defined(_MSC_VER) || defined(_WIN32))
#define __WINDOWS__
#endif

#ifdef __WINDOWS__
#define __STDCALL __stdcall
#define __CDECL__	__cdecl
#if defined SHEEPS_EXPORTS
#define LSHEEPS_API __declspec(dllexport)
#else
#define LSHEEPS_API __declspec(dllimport)
#endif
#else
#define __STDCALL
#define __CDECL__
#define LSHEEPS_API
#endif // __WINDOWS__

#ifdef __cplusplus
extern "C"
{
#endif

	int __STDCALL SheepsLuaTest(const char* file);
	int __STDCALL TaskManagerRunWithLua(int projectid, const char* groupid, bool server, const char* ip, int port, const char* configfile, const char* scriptfile);

#ifdef __cplusplus
}
#endif

#endif // !__LSHEEPS_RUN_H__
