/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#ifndef __LUA_PROTOCOL_H__
#define __LUA_PROTOCOL_H__
#include "lsheeps.h"
#include "lcommon.h"
#include "lua_3rd.h"

extern const char* main_lua;
extern const char* config_lua;

#define USER_CREATE_LUAVM_INIT

class LuaProtocol :
	public ReplayProtocol
{
public:
	LuaProtocol();
	~LuaProtocol();

public:
	struct lua_State* PL = NULL;
	unsigned char luaLastGc = 0;
#ifndef USER_CREATE_LUAVM_INIT
	unsigned char env_inited = 0;
#endif

public:
	void EventStart();
	void EventConnectOpen(const char* ip, int port, PROTOCOL protocol);
	void EventConnectClose(const char* ip, int port, PROTOCOL protocol);
	void EventConnectSend(const char* ip, int port, const char* content, int clen, PROTOCOL protocol);
	void EventTimeOut();
	void EventStop();
	void EventStopSignal();
	void EventConnectMade(HSOCKET hsock);
	void EventConnectFailed(HSOCKET hsock, int err);
	void EventConnectClosed(HSOCKET hsock, int err);
	void EventConnectRecved(HSOCKET hsock, const char* data, int len);
};

#endif // !__LUA_PROTOCOL_H__