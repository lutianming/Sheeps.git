/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#ifndef __LSHEEPS_H__
#define __LSHEEPS_H__
#include "stdio.h"
#include "lua.hpp"
#include "AgentManager.h"

int register_sheeps_api(lua_State* L);

#endif // !__LSHEEPS_H__


