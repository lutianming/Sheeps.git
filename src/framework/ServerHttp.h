#pragma once
#include "ServerProtocol.h"
#include "ServerHttpProxy.h"

int CheckHttpRequest(HSOCKET hsock, ServerProtocol* proto, const char* data, int len);