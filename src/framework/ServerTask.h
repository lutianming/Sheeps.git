/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#ifndef _SERVER_CONSOLE_H_
#define _SERVER_CONSOLE_H_

#if !defined(__WINDOWS__) && (defined(WIN32) || defined(WIN64) || defined(_MSC_VER) || defined(_WIN32))
#define __WINDOWS__
#endif

#include "ServerProtocol.h"
#include <map>
#include <vector>
#include <string>

extern int api_samples;

enum:char {
	STATE_UNSTART = 0,
	STATE_RUNING,
	STATE_BALANCE,
	STATE_STOP
};

enum:char{
	STAGE_INIT = 0,
	STAGE_LOOP,
	STAGE_CLEAN,
	STAGE_OVER
};

typedef struct {
	char srcAddr[48];
	char dstAddr[48];
}Readdr, *HREADDR;

typedef struct {
	long long	record_time;
	uint8_t		event;
	int			protocol;
	int			port;
	char		ip[40];
	long long	sessionid;
	char*		content;
	char*		note;
	int			content_len;
	int			note_len;
}RecordMsg, * HRECORDMSG;

typedef struct {
	time_t	last_report_write;
	int		zero_count;
	std::vector<std::string>* vecData;
}TaskReportOnline;

typedef struct {
	time_t	last_report_write;
	std::mutex* vecLock;
	std::vector<std::string>* vecData;
}TaskReportError;

typedef struct {
	long total;
	long success;
	long faile;
}ReportInfoConnect;

typedef struct {
	time_t	last_report_write;
	long	total;
	std::map<std::string, ReportInfoConnect*>* info;
	std::vector<std::string>* vecData;
}TaskReportConnect;

typedef struct {
	long up;
	long down;
	long net_up;
	long net_down;
}ReportInfoNet;

typedef struct {
	time_t	last_report_write;
	long	total;
	int		zero_count;
	std::map<std::string, ReportInfoNet*>* info;
	std::vector<std::string>* vecData;
}TaskReportNetflow;

typedef struct {
	long long send_flow;
	long long recv_flow;
	long send_count;
	long recv_count;
	long error;
	long success;
	long sorted;
	std::vector<int>* vecData;
}ReportInfoApi;

typedef struct {
	long value;
	char stype;
	int  space_time;
	int  last_time;
	long long start_time;
}ReportInfoCounter;

typedef struct {
	std::map<std::string, ReportInfoCounter*>* info;
	std::vector<std::string>* vecData;
}TaskReportCounter;

typedef struct {
	std::list<RecordMsg*>* tempMsg;
	time_t startRecord;
	time_t startReal;
	int32_t startRow;
	bool stopPushRecord;
	sqlite3* dbConn;
	char* dbsql;
	int sqllen;
}PushMessage;

typedef struct {
	uint8_t stage;
	bool	enforcestop;  //强制停止
	uint8_t	report_log;
	long	UserActiveCount;
	long	UserDeadCount;
	long	UserOnlineCount;

	long	case_time_len;
	long	run_success;
	long	run_failed;
	TaskReportOnline online;
	TaskReportError error;
	TaskReportConnect connect;
	TaskReportNetflow netpack;
	std::map<std::string, ReportInfoApi*>* api;
	TaskReportCounter counter;

	time_t		StartTime;
	time_t		LastPushUserTime;
	PushMessage pushmsg;
}ServerTaskRun, * hServerTaskRun;

typedef struct {
	int			taskID;
	char		taskCode[4];
	uint8_t		projectID;
	int32_t		userExecuteCycle;
	char		projectName[32];
	int32_t		totalUser;
	int32_t		onceUser;
	uint16_t	spaceTime;
	uint8_t		loopMode; //0循环 1不循环 2实时回放
	bool		ignoreErr;
	bool		report_log;
	uint8_t		logLevel;
	char		groupID[32];
	char*		parms1;
	char*		parms2;

	uint8_t		taskState;
	char		taskDes[64];
	char		caseDir[32];
	char		dbName[32];
	std::list<Readdr*>* replayAddr;
	std::map<std::string, Readdr*> *changeAddr;

	hServerTaskRun	taskrun;
	int	run_number;
}ServerTaskConfig, * hServerTaskConfig;

extern std::map<int, hServerTaskConfig>* ServerTaskCfg;
extern std::mutex* ServerTaskCfgLock;

void task_code_init();
void task_code_get(char* buf);
int task_id_pool_get_id();
void task_id_pool_push_back(int task_id);

hServerTaskConfig get_server_taskcfg_by_id(int task_id);
void insert_server_taskcfg(int task_id, hServerTaskConfig taskcfg);
void delete_server_taskcfg(int task_id);

hServerTaskConfig task_report_timeout(int task_id, int run_number);

void task_user_dead(int time, hServerTaskConfig taskcfg, int machine_id, int user_number, int run_time, const char* errmsg);
void task_user_online_count(hServerTaskConfig taskcfg, int count);
void task_user_connect_result(hServerTaskConfig taskcfg, const char* addr, int success, int faile);
void task_user_netflow(hServerTaskConfig taskcfg, const char* addr, int up, int down, int net_up, int net_down);
void task_user_api(hServerTaskConfig taskcfg, const char* api_name, int send_flow, int send_count, int recv_flow, int recv_count, int error_count, cJSON* response_time);
void task_user_counter(hServerTaskConfig taskcfg, const char* key, int val, int stype, int space_time);

void task_run(hServerTaskConfig taskcfg);
void task_push_over(hServerTaskRun taskrun, hServerTaskConfig taskcfg);

void taskrun_clear(hServerTaskRun taskrun);

#endif // !_SERVER_CONSOLE_H_