#pragma once
#include "ServerProtocol.h"

#define	HTTP_PROTOCOL 0x08
#define HTTPS_RPOTOCOL 0x09

int do_http_cors_proxy(HSOCKET hsock, ServerProtocol* proto, const char* data, int len);
int do_http_proxy(HSOCKET hsock, ServerProtocol* proto, char* url, const char* data, int len);
int do_https_proxy(HSOCKET hsock, ServerProtocol* proto, char* url, const char* data, int len);

void http_proxy_connection_made(HSOCKET hsock, ServerProtocol* proto);
void http_proxy_connection_faile(HSOCKET hsock, ServerProtocol* proto);
void http_proxy_connection_closed(HSOCKET hsock, ServerProtocol* proto);

void http_proxy_server_connection_recved(HSOCKET hsock, ServerProtocol* proto, const char* data, int len);
int http_proxy_client_connection_recved(HSOCKET hsock, ServerProtocol* proto, const char* data, int len);