﻿/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#include "framework.h"
#include "SheepsMain.h"
#include "SheepsFactory.h"
#include "ServerHook.h"
#include <iostream>

#ifdef __WINDOWS__
#include "shellapi.h"
#ifdef _DEBUG
#ifndef _WIN64
#pragma comment(lib,".\\..\\Win32\\sheeps\\Debug\\third.lib")
#pragma comment(lib,".\\..\\Win32\\sheeps\\Debug\\network.lib")
#else
#pragma comment(lib, ".\\..\\X64\\sheeps\\Debug\\third.lib")
#pragma comment(lib, ".\\..\\X64\\sheeps\\Debug\\network.lib")
#endif // _WIN32

#else
#ifndef _WIN64
#pragma comment(lib, ".\\..\\Win32\\sheeps\\release\\third.lib")
#pragma comment(lib, ".\\..\\Win32\\sheeps\\release\\network.lib")
#else
#pragma comment(lib, ".\\..\\X64\\sheeps\\release\\third.lib")
#pragma comment(lib, ".\\..\\X64\\sheeps\\release\\network.lib")
#endif // _WIN32
#endif
#endif // __WINDOWS__

char ServerInfoString[1024] = { 0x0 };

SheepsAccepter* SheepsAccepterObj = NULL;

static void actor_timer(HTIMER timer, BaseWorker* worker, void* user_data) {
	SheepsAccepterObj->TimeOut();
}

static int SheepsAccepterRun(const char* ip, int listenPort){
	if (NULL == SheepsAccepterObj){
		SheepsAccepterObj = new(std::nothrow) SheepsAccepter();
		if (SheepsAccepterObj == NULL){
			printf("%s:%d Sheeps accepter create failed!\n", __func__, __LINE__);
			return -1;
		}
	}
	SheepsAccepterObj->Init(listenPort);
	if (SheepsAccepterObj->listen(ip, listenPort)){
		printf("%s:%d Sheeps accepter start failed!\n", __func__, __LINE__);
		return -2;
	}
	SheepsAccepterObj->Inited();
	return 0;
}

int __STDCALL SheepsRun(const char* ip, int port){
	if (ActorStart(0)) return -1;
	http_client_driver_regist();
	if (SheepsAccepterRun(ip, port)) return -2;
	TimerCreate(NULL, NULL, 1000, 1000, actor_timer);
	return 0;
}

int __STDCALL SheepsRunServerManager(const char* ip, int port) {
	int ret = SheepsRun(ip, port);
	if (ret < 0){
		system("pause");
		return -2;
	}

#ifdef __WINDOWS__
	char url[32] = { 0x0 };
#define clear_screen "CLS"
#else
#define clear_screen "clear"
	if (ShowConsole == false) {
		while (1) { //如果linux下不需要控制台，直接休眠即可
			TimeSleep(30000);
		}
	}
#endif

	char in[8] = { 0x0 };
	char host[64] = { 0x0 };
	if (strchr(ip, ':')) {
		snprintf(host, sizeof(host), "[%s]:%d", ip, port);
	}
	else {
		snprintf(host, sizeof(host), "%s:%d", ip, port);
	}
	while (true){
		system(clear_screen);
		printf("欢迎使用SuperSheeps\n了解更多http://supersheeps.cn\n正在运行控制端模式，后台访问：http://[::1]:%d\n\n",
			port);
		printf("控   制   端：%s\nsocks5  代理：%s\nhttps(s)代理：%s\n后        台：%s\n", host, host, host, host);
		printf("\n%s\n代理扩展：%s\n", ServerInfoString, proxy_hook?"开":"关");
		printf("\n选择:【0.管理后台】【1.主页】【2.重载控制端脚本】【3.代理扩展】【Quit退出】\n操作：");
		fgets(in, 8, stdin);
		if (in[0] == 'Q' && in[1] == 'u' && in[2] == 'i' && in[3] == 't')
			break;
		else if (in[0] == '3') {
			proxy_hook = !proxy_hook;
		}
		else if (in[0] == '2') {
			server_hook_reinit();
		}
		else if (in[0] == '1') {
#ifdef __WINDOWS__
			snprintf(url, sizeof(url), "http://supersheeps.cn/");
			HINSTANCE res = ShellExecuteA(NULL, "open", url, NULL, NULL, SW_SHOWNORMAL);
#endif
		}
		else if (in[0] == '0') {
#ifdef __WINDOWS__
			snprintf(url, sizeof(url), "http://127.0.0.1:%d/", port);
			HINSTANCE res = ShellExecuteA(NULL, "open", url, NULL, NULL, SW_SHOWNORMAL);
#endif
		}
	}
	return 0;
}