#pragma once
/*
*	Copyright(c) 2020 lutianming email：641471957@qq.com
*
*	Sheeps may be copied only under the terms of the GNU Affero General Public License v3.0
*/

#include "stdio.h"
#include "lua.hpp"

#define _HookProtoName "_hook_proto_name"
#define _HookProtoPpid "_hook_proto_ppid"

int lua_State_Set_Package_Path(lua_State* L, const char* path);

#define SOCKET_LISTEN 0
#define SOCKET_CONNECT 1
int create_socket_content(lua_State* L, void* handle, int type);

int register_hook_api(lua_State* L);

LUALIB_API void luaL_open_pasture_socket(lua_State* L);
