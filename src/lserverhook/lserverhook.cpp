﻿#include "LuaHookProtocol.h"

#ifdef __WINDOWS__
#ifdef _DEBUG
#ifndef _WIN64
#pragma comment(lib,"..\\Win32\\sheeps\\Debug\\framework.lib")
#pragma comment(lib,"..\\Win32\\sheeps\\Debug\\lua.lib")
#else
#pragma comment(lib, "..\\X64\\sheeps\\Debug\\framework.lib")
#pragma comment(lib, "..\\X64\\sheeps\\Debug\\lua.lib")
#endif // _WIN32
#else
#ifndef _WIN64
#pragma comment(lib, "..\\Win32\\sheeps\\Release\\framework.lib")
#pragma comment(lib, "..\\Win32\\sheeps\\Release\\lua.lib")
#else
#pragma comment(lib, "..\\X64\\sheeps\\Release\\framework.lib")
#pragma comment(lib, "..\\X64\\sheeps\\Release\\lua.lib")
#endif // _WIN32
#endif
#endif // __WINDOWS__

static ServerHookProtocol* lua_server_hook_create()
{
	ServerHookProtocol* proto = new(std::nothrow) LuaHookProtocol;
	return proto;
}

void server_hook_set() {
	server_hook_create_func = lua_server_hook_create;
}
