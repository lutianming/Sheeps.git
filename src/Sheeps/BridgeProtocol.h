#pragma once

#if !defined(__WINDOWS__) && (defined(WIN32) || defined(WIN64) || defined(_MSC_VER) || defined(_WIN32))
#define __WINDOWS__
#endif

#ifdef __WINDOWS__
#define __STDCALL __stdcall
#define __CDECL__	__cdecl
#if defined SHEEPS_EXPORTS
#define Sheeps_API __declspec(dllexport)
#else
#define Sheeps_API __declspec(dllimport)
#endif
#else
#define STDCALL
#define CDECL
#define Sheeps_API

#include <string.h>
#endif // __WINDOWS__
#include "AgentManager.h"

typedef int (__STDCALL *func_event_start_callback)(void* user, int task_id, int user_number);
typedef int (__STDCALL *func_event_connect_open_callback)(void* user, const char* ip, int port, PROTOCOL protocol);
typedef int (__STDCALL *func_event_connect_made_callback)(void* user, HSOCKET hsock);
typedef int (__STDCALL *func_event_connect_failed_callback)(void* user, HSOCKET hsock, int err);
typedef int (__STDCALL *func_event_connect_send_callback)(void* user, const char* ip, int port, const char* data, int clen, PROTOCOL protocol);
typedef int (__STDCALL *func_event_connect_recved_callback)(void* user, HSOCKET hsock, const char* data, int len);
typedef int (__STDCALL *func_event_connect_close_callback)(void* user, const char* ip, int port, PROTOCOL protocol);
typedef int (__STDCALL *func_event_connect_closed_callback)(void* user, HSOCKET hsock, int err);
typedef int (__STDCALL *func_event_timeout_callback)(void* user);
typedef int (__STDCALL *func_event_stop_callback)(void* user, const char* errmsg);

typedef void*	(__STDCALL *func_task_user_create)(void* user_handle, int task_id);
typedef int		(__STDCALL *func_task_start)(int taskid, int protject_id, int machine_id, int run_number, const char* parms);
typedef int		(__STDCALL *func_task_stop)(int taskid);

class BirdgeProtocol :public ReplayProtocol
{
public:
	void* user = NULL;
	BirdgeProtocol();
	~BirdgeProtocol();
	void EventStart();
	void EventConnectOpen(const char* ip, int port, PROTOCOL protocol);
	void EventConnectMade(HSOCKET hsock);
	void EventConnectFailed(HSOCKET hsock, int err);
	void EventConnectClose(const char* ip, int port, PROTOCOL protocol);
	void EventConnectClosed(HSOCKET hsock, int err);
	void EventConnectSend(const char* ip, int port, const char* content, int len, PROTOCOL protocol);
	void EventConnectRecved(HSOCKET hsock, const char* data, int len);
	void EventTimeOut();
	void EventStop();
	void EventStopSignal();
};

#ifdef __cplusplus
extern "C"
{
#endif
	Sheeps_API void __STDCALL set_task_callback(func_task_user_create create, func_task_start start, func_task_stop stop);
	Sheeps_API void __STDCALL set_user_event_callback(func_event_start_callback start, func_event_connect_open_callback oepn, func_event_connect_made_callback made,
		func_event_connect_failed_callback failed, func_event_connect_send_callback send, func_event_connect_recved_callback recv,
		func_event_connect_close_callback close, func_event_connect_closed_callback closed, func_event_timeout_callback timeout,
		func_event_stop_callback stop);
	Sheeps_API int __STDCALL TaskManagerRunWithBridge(int projectid, const char* groupid, int local_server, const char* configfile);
#ifdef __cplusplus
}
#endif